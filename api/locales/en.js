{
  "ERROR_DATOS_OBLIGATORIOS_REGISTRO": "Mandatory data missing.",
  "ERROR_DATOS_REGISTRO": "Invalid data for registration.",
  "ERROR_EXISTENCIA_USUARIO": "Invalid user for registration.",
  "ERROR_USUARIO_INEXISTENTE":"User not found",
  "ERROR_USUARIO_LOGIN": "Authentication failed. User not found.",
  "ERROR_PASSWORD_LOGIN": "Authentication failed. Wrong password.",
  "ERROR_USUARIO_NO_AUTORIZADO": "Unauthorized user.",
  "ERROR_CONEXION_BASEDATOS": "Database server error.",
  "ERROR_SERVIDOR": "Server error.",
  "ERROR_CUENTA_INEXISTENTE": "Account not found.",
  "ERROR_CUENTAS_INEXISTENTE": "Accounts not found.",
  "ERROR_TIPO_MOVIMIENTO": "Invalid transaction type",
  "ERROR_MOVIMIENTO_INEXISTENTE": "Transaction not found.",
  "ERROR_MOVIMIENTOS_INEXISTENTE": "Transactions not found.",
  "ERROR_MOVIMIENTO_INBORRABLE": "Transaction cannot be removed.",
  "TIPO_MOVIMIENTO_NOMINA": "Salary payment",
  "TIPO_MOVIMIENTO_ADEUDO": "Debit",
  "TIPO_MOVIMIENTO_CAJERO": "Cash withdrawal in atm",
  "TIPO_MOVIMIENTO_IMPUESTO": "Tax payment",
  "TIPO_MOVIMIENTO_TRANS.REALIZADA": "Transfer completed",
  "TIPO_MOVIMIENTO_TRANS.RECIBIDA": "Transfer received",
  "TIPO_MOVIMIENTO_VALORES": "Stocks-dividends recived",
  "TIPO_MOVIMIENTO_OTROS": "Others",
  "TIPO_MOVIMIENTO_ANULACION": "Return of movement"
}
