'use strict';
module.exports = function(app) {
//  var todoList = require('../controllers/todoListController');
  var userHandlers = require('../controllers/userController');
  var loginHandlers = require('../controllers/loginController');
  var accountHandlers = require('../controllers/accountController');
  var transactionHandlers = require('../controllers/transactionController');
  var dropoutControllers = require('../controllers/dropoutController');

  app.route('/sign_up')
    .post(loginHandlers.sign_up, userHandlers.create);
  app.route('/log_in')
    .post(loginHandlers.log_in, userHandlers.authenticate);
  app.route('/change_password')
    .put(userHandlers.loginRequired, loginHandlers.change_password);
  app.route('/log_out')
    .get(userHandlers.loginRequired, userHandlers.log_out);

  app.route('/clientes')
    .get(userHandlers.loginRequired, userHandlers.read)
    .put(userHandlers.loginRequired, userHandlers.update)
    .delete(userHandlers.loginRequired, dropoutControllers.delete);

  app.route("/cuentas")
    .post(userHandlers.loginRequired, accountHandlers.create_account)
    .get(userHandlers.loginRequired, accountHandlers.list_all_accounts);

  app.route("/cuentas/:accountId")
    .get(userHandlers.loginRequired, accountHandlers.read_account)
    .put(userHandlers.loginRequired, accountHandlers.update_account)
    .delete(userHandlers.loginRequired, accountHandlers.delete_account);

  app.route("/cuentas/:accountId/movimientos")
    .post(userHandlers.loginRequired, transactionHandlers.create_transaction)
    .get(userHandlers.loginRequired, transactionHandlers.list_all_transactions);

  app.route("/cuentas/:accountId/movimientos/:transactionId")
    .get(userHandlers.loginRequired, transactionHandlers.read_transaction)
    .put(userHandlers.loginRequired, transactionHandlers.update_transaction)
    .delete(userHandlers.loginRequired, transactionHandlers.delete_transaction);

  app.route("/tiposmovimientos")
    .get(transactionHandlers.list_all_transactionTypes);
};
