'use strict';

//ES00 0000 YYYY 0000 XXXX
exports.addAccount = function(userId, accountId) {
    return "ES000000"+this.completarConCeros(userId, 4)+"0000"+this.completarConCeros(accountId, 4);
};

exports.completarConCeros = function (lid, longitud) {
  var id = ""+lid;
  var length = id.length;
  var cid = "";
  while (longitud -cid.length-length>0) {
    cid = cid+"0";
  }
  cid = cid+id;
  return cid;
};
