'use strict';
//Validamos que el usuario sea una dirección de correo válida
exports.userValidation = function(user) {
  if ( (/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(user)) ) {
    return true;
  } else {
    return false;
  };
};
//Validamos que el usuario tengo entre 8 y 16 carácteres alfanuméricos, al menos 1 número y 1 mayuscula y 1 minuscula
exports.passwordValidation = function(password) {
  if ( (/^(?=\w*\d)(?=\w*[A-Z])(?=\w*[a-z])\S{8,16}$/.test(password)) ) {
    return true;
  } else {
    return false;
  };
};
