'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var TransactionSchema = new Schema({
  transactionId: {
    type: String,
    unique: true,
    required: true,
    trim: true
  },
  accountId: {
    type: Number,
    required: true
  },
  transactionOrder: {
    type: Number,
    required: true
  },
  transactionType: {
    type: [{
      type: String,
      enum: ['NOMINA', 'ADEUDO', 'CAJERO', 'IMPUESTO','TRANS.REALIZADA','TRANS.RECIBIDA','VALORES', 'OTROS', 'ANULACION']
    }],
  },
  transactionRelated: {
    type: String,
    default: ""
  },
  transactionFlowType: {
    type: String,
    enum: ['INCOME', 'EXPENSE']
  },
  transactionAmount: {
    type: Number
  },
  countUpdatedBalance: {
    type: Number
  },
  additionalInformation: {
    type: String,
    trim: true
  },
  created: {
    type: Date,
    default: Date.now
  }
});

mongoose.model('Transaction', TransactionSchema);
