'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var bcrypt = require('bcrypt');

var UserSchema = new Schema({
  userId: {
    type: Number,
    unique: true,
    required: true,
  },
  email: {
    type: String,
    unique: true,
    lowercase: true,
    trim: true,
    required: true
  },
  firstName: {
    type: String,
    trim: true,
    required:true
  },
  lastName: {
    type: String,
    trim: true,
    required:true
  },
  active: {
    type: Boolean,
    default: true
  },
  created: {
    type: Date,
    default: Date.now
  },
  logout: {
    type: Date,
    default: Date.now
  }
});



mongoose.model('User', UserSchema);
