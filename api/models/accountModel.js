'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var AccountSchema = new Schema({
  accountId: {
    type: Number,
    unique: true,
    required: true,
  },
  userId: {
    type: Number,
    required: true
  },
  accountNumber: {
    type: String,
    unique: true,
    trim: true,
    required: true
  },
  countBalance: {
    type: Number,
    default: 0
  },
  accountAlias: {
    type: String,
    trim: true,
    default: ""
  },
  created: {
    type: Date,
    default: Date.now
  }
});

mongoose.model('Account', AccountSchema);
