'use strict';

var mongoose = require('mongoose');
var jwt = require('jsonwebtoken');
var i18n = require("i18n-2");
var validation = require('../utils/validation');

var User = mongoose.model('User');

exports.create = function(req, res) {
  var newUser = new User(req.body);
  User.findOne({}, 'userId', {
    sort: {
      userId: -1
    }
  }, function(err, lastUser) {
    if (err) {
      console.error("ERROR FINDONE crear usuario MONGO: " + err.stack);
      return res.status(500).json({
        message: req.i18n.__('ERROR_SERVIDOR')
      });
    } else {
      if (!lastUser) {
        newUser.userId = 1;
      } else {
        newUser.userId = lastUser.userId + 1;
      }
      newUser.save(function(err, user) {
        if (err) {
          console.error("ERROR SAVE crear usuario MONGO: " + err.stack);
          return res.status(500).send({
            message: req.i18n.__('ERROR_SERVIDOR')
          });
        } else {
          return res.status(201).json({
            token: jwt.sign({
              exp: Math.floor(Date.now() / 1000) + (60 * 60),
              userId: user.userId,
              email: user.email,
              firstName: user.firstName,
              lastName: user.lastName,
              active: user.active,
              created: user.created,
              logout: user.logout,
              _id: user._id
            }, 'TECHURUTHAUTh')
          });
        }
      });
    }
  });
};

exports.authenticate = function(req, res) {
  User.findOne({
    email: req.body.email
  }, function(err, user) {
    if (err) {
      console.error("ERROR FINDONE autenticar MONGO: " + err.stack);
      return res.status(500).json({
        message: req.i18n.__('ERROR_SERVIDOR')
      });
    }
    if (!user) {
      return res.status(400).json({
        message: req.i18n.__('ERROR_USUARIO_LOGIN')
      });
    } else {
      return res.json({
        token: jwt.sign({
          exp: Math.floor(Date.now() / 1000) + (60 * 60),
          userId: user.userId,
          email: user.email,
          firstName: user.firstName,
          lastName: user.lastName,
          active: user.active,
          created: user.created,
          logout: user.logout,
          _id: user._id
        }, 'TECHURUTHAUTh')
      });
    }
  });
};

exports.loginRequired = function(req, res, next) {
  if (req.user) {
    User.findOne({
      email: req.user.email
    }, function(err, user) {
      if (err) {
        console.error("ERROR FINDONE login required MONGO: " + err.stack);
        return res.status(500).json({
          message: req.i18n.__('ERROR_SERVIDOR')
        });
      }
      if (req.user && req.user.logout && user && user.logout) {
        var request = new Date(req.user.logout);
        var basedatos = new Date(user.logout);
        if (request < basedatos) {
          return res.status(401).json({
            message: req.i18n.__('ERROR_USUARIO_NO_AUTORIZADO')
          });
        } else {
          next();
        }
      } else {
        return res.status(401).json({
          message: req.i18n.__('ERROR_USUARIO_NO_AUTORIZADO')
        });
      }
    });
  } else {
    return res.status(401).json({
      message: req.i18n.__('ERROR_USUARIO_NO_AUTORIZADO')
    });
  }
};

exports.read = function(req, res) {
  User.findOne({
    email: req.user.email
  }, function(err, user) {
    if (err) {
      console.error("ERROR FINDONE read usuario MONGO: " + err.stack);
      return res.status(500).json({
        message: req.i18n.__('ERROR_SERVIDOR')
      });
    }
    if (!user) {
      return res.status(404).json({
        message: req.i18n.__('ERROR_USUARIO_INEXISTENTE')
      });
    } else {
      return res.json(user);
    }
  });
};

exports.update = function(req, res) {
  var toSave = true;
  User.findOne({
    email: req.user.email
  }, function(err, user) {
    if (err) {
      console.error("ERROR FINDONE update usuario MONGO: " + err.stack);
      return res.status(500).json({
        message: req.i18n.__('ERROR_SERVIDOR')
      });
    }
    if (!user) {
      toSave = false;
      return res.status(404).json({
        message: req.i18n.__('ERROR_USUARIO_INEXISTENTE')
      });
    } else {
      if (req.body.firstName) {
        user.firstName = req.body.firstName;
      }
      if (req.body.lastName) {
        user.lastName = req.body.lastName;
      }
      if (req.body.active) {
        user.active = req.body.active;
      }
      if (toSave) {
        user.save(function(err, user) {
          if (err) {
            console.error("ERROR SAVE update usuario MONGO: " + err.stack);
            return res.status(500).json({
              message: req.i18n.__('ERROR_SERVIDOR')
            });
          } else {
            return res.json(user);
          }
        });
      }
    }
  });
};

exports.log_out = function(req, res) {
  User.findOne({
    email: req.user.email
  }, function(err, user) {
    if (err) {
      console.error("ERROR FINDONE logout MONGO: " + err.stack);
      return res.status(500).json({
        message: req.i18n.__('ERROR_SERVIDOR')
      });
    }
    if (!user) {
      return res.status(404).json({
        message: req.i18n.__('ERROR_USUARIO_INEXISTENTE')
      });
    } else {
      user.logout = Date.now();
      user.save(function(err, user) {
        if (err) {
          console.error("ERROR SAVE logout MONGO: " + err.stack);
          return res.status(500).json({
            message: req.i18n.__('ERROR_SERVIDOR')
          });
        } else {
          return res.status(200).send();
        }
      });
    }
  });
};
