'use strict';

var jwt = require('jsonwebtoken');
var i18n = require("i18n-2");
var bcrypt = require('bcrypt');
var validation = require('../utils/validation');
var pg = require('pg');
//var urlUsuarios = "postgres://docker:docker@localhost:5433/dblogin";
var urlUsuarios = "postgres://docker:docker@postgresql:5432/dblogin";
//CAMBIAR TAMBIEN EN LOGINCONTROLLER

var userHandlers = require('../controllers/userController');
var mongoose = require('mongoose');
var User = mongoose.model('User');
var Transaction = mongoose.model('Transaction');
var Account = mongoose.model('Account');

exports.delete = function(req, res) {
  var postgreClient = new pg.Client(urlUsuarios);
  postgreClient.connect((err) => {
    if (err) {
      console.error("ERROR CONEXION POSTGRESQL en delete:" + err.stack);
      return res.status(503).json({
        message: req.i18n.__('ERROR_CONEXION_BASEDATOS')
      });
    } else {
      //Borro de dblogin
      var query = postgreClient.query('DELETE FROM usuarios WHERE usuario = $1', [req.user.email], (err, result) => {
        postgreClient.close;
        if (err) {
          console.error("ERROR QUERY DELETE usuarios POSTGRESQL en delete:" + err.stack);
          return res.status(500).json({
            message: req.i18n.__('ERROR_SERVIDOR')
          });
        } else {
          //Busco usuario de mLab
          User.findOne({
            email: req.user.email
          }, function(err, user) {
            if (err) {
              console.error("ERROR FINDONE delete user MONGO: " + err.stack);
            }
            if (user) {
              //Busco las cuentas del usuario
              Account.find({
                userId: req.user.userId,
              }, ['accountId'], function(err, result) {
                if (err) {
                  console.error("ERROR FIND borrar cuentas MONGO: " + err.stack);
                } else {
                  if (result.length != 0) {
                    //Borro los movimientos de cada cuenta
                    for (var i = 0; i < result.length; i++) {
                      Transaction.remove({
                        accountId: result[i].accountId
                      }, function(err, transaction) {
                        if (err) {
                          console.error("ERROR REMOVE en delete_transaction MONGO: " + err.stack);
                        }
                      });
                    }
                    //Borro las cuentas
                    Account.remove({
                      userId: req.user.userId
                    }, function(err, result) {
                      if (err) {
                        console.error("ERROR REMOVE borrar cuentas MONGO: " + err.stack);
                      }
                    });
                  }
                }
              });
              user.remove();
            }
          });
          return res.status(204).send();
        }
      });
    };
  });
};
