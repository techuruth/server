'use strict';

var mongoose = require('mongoose');
var i18n = require("i18n-2");
var accountUtility = require('../utils/accountUtilities');

var transactionHandlers = require('../controllers/transactionController');

var Account = mongoose.model('Account');

exports.create_account = function(req, res) {
  //Las cuentas tendrán el formato  ES00 0000 YYYY 0000 XXXX donde YYYY es el código de cliente y XXXX el codigo de cuenta
  var newAccount = new Account(req.body);
  newAccount.userId = req.user.userId;
  newAccount.accountAlias = req.body.accountAlias;
  Account.findOne({}, ['accountId', 'accountNumber'], {
    sort: {
      accountId: -1
    }
  }, function(err, lastAccount) {
    if (err) {
      console.error("ERROR FINDONE crear cuenta MONGO: " + err.stack);
      return res.status(500).json({
        message: req.i18n.__('ERROR_SERVIDOR')
      });
    } else {
      if (lastAccount) {
        //    return res.json(lastAccount);
        newAccount.accountId = parseInt(lastAccount.accountId) + 1;
      } else {
        newAccount.accountId = 1;
      }
      newAccount.accountNumber = accountUtility.addAccount(newAccount.userId, newAccount.accountId);
      newAccount.save(function(err, account) {
        if (err) {
          console.error("ERROR SAVE crear cuenta MONGO: " + err.stack);
          return res.status(500).json({
            message: req.i18n.__('ERROR_SERVIDOR')
          });
        } else {
          return res.status(201).json(account);
        }
      });
    }
  });
};

exports.list_all_accounts = function(req, res) {
  Account.find({
    userId: req.user.userId
  }, ['accountId', 'accountNumber', 'accountAlias', 'countBalance'], {
    sort: {
      created: 1
    }
  }, function(err, result) {
    if (err) {
      console.error("ERROR FIND listar cuentas MONGO: " + err.stack);
      return res.status(500).json({
        message: req.i18n.__('ERROR_SERVIDOR')
      });
    } else {
      if (result) {
        return res.json(result);
      } else {
        return res.status(404).json({
          message: req.i18n.__('ERROR_CUENTAS_INEXISTENTE')
        });
      }
    }
  });
};

exports.read_account = function(req, res) {
  Account.findOne({
    userId: req.user.userId,
    accountId: req.params.accountId
  }, ['accountId', 'accountNumber', 'accountAlias', 'countBalance'], function(err, account) {
    if (err) {
      console.error("ERROR FINDONE buscar cuenta MONGO: " + err.stack);
      return res.status(500).json({
        message: req.i18n.__('ERROR_SERVIDOR')
      });
    } else {
      if (account) {
        return res.json(account);
      } else {
        return res.status(404).json({
          message: req.i18n.__('ERROR_CUENTA_INEXISTENTE')
        });
      }
    }
  });
}

exports.update_account = function(req, res) {
  Account.findOneAndUpdate({
    userId: req.user.userId,
    accountId: req.params.accountId
  }, {
    accountAlias: req.body.accountAlias
  }, {
    new: true,
    fields: ['accountId', 'accountNumber', 'accountAlias', 'countBalance']
  }, function(err, account) {
    if (err) {
      console.error("ERROR FINDONEANDUPDATE modificar cuenta MONGO: " + err.stack);
      return res.status(500).json({
        message: req.i18n.__('ERROR_SERVIDOR')
      });
    } else {
      if (account) {
        return res.json(account);
      } else {
        return res.status(404).json({
          message: req.i18n.__('ERROR_CUENTA_INEXISTENTE')
        });
      }
    }
  });
};

exports.delete_account = function(req, res) {
  Account.remove({
    userId: req.user.userId,
    accountId: req.params.accountId
  }, function(err, result) {
    if (err) {
      console.error("ERROR REMOVE borrar cuenta MONGO: " + err.stack);
      return res.status(500).json({
        message: req.i18n.__('ERROR_SERVIDOR')
      });
    } else {
      if (result.result.n != 0) {
        transactionHandlers.delete(req, res);
        return res.status(204).send();
      } else {
        return res.status(404).json({
          message: req.i18n.__('ERROR_CUENTA_INEXISTENTE')
        });
      }
    }
  });
};
