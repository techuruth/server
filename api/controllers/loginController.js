'use strict';

var jwt = require('jsonwebtoken');
var i18n = require("i18n-2");
var bcrypt = require('bcrypt');
var validation = require('../utils/validation');
var pg = require('pg');
//var urlUsuarios = "postgres://docker:docker@localhost:5433/dblogin";
var urlUsuarios = "postgres://docker:docker@postgresql:5432/dblogin";
//CAMBIAR TAMBIEN EN DROPOUTCONTROLLER

var userHandlers = require('../controllers/userController');
var mongoose = require('mongoose');
var User = mongoose.model('User');

exports.sign_up = function(req, res, next) {
  if (!req.body.lastName || !req.body.firstName || !req.body.email || !req.body.password) {
    return res.status(400).json({
      message: req.i18n.__('ERROR_DATOS_OBLIGATORIOS_REGISTRO')
    });
  } else {
    if (!validation.userValidation(req.body.email) || !validation.passwordValidation(req.body.password)) {
      return res.status(400).json({
        message: req.i18n.__('ERROR_DATOS_REGISTRO')
      });
    } else {
      var postgreClient = new pg.Client(urlUsuarios);
      postgreClient.connect((err) => {
        if (err) {
          console.error("ERROR CONEXION POSTGRESQL en sign_up:" + err.stack);
          return res.status(500).json({
            message: req.i18n.__('ERROR_CONEXION_BASEDATOS')
          });
        } else {
          var hash_password = bcrypt.hashSync(req.body.password, 10);
          var query = postgreClient.query('INSERT INTO usuarios (usuario,password) VALUES($1,$2);', [req.body.email.toLowerCase(), hash_password], (err, result) => {
            postgreClient.close;
            if (err) {
              console.error("ERROR QUERY INSERT usuarios POSTGRESQL en sign_up:" + err.stack);
              if (err.code = '23505') {
                return res.status(400).json({
                  message: req.i18n.__('ERROR_EXISTENCIA_USUARIO')
                });
              } else {
                return res.status(500).json({
                  message: req.i18n.__('ERROR_SERVIDOR')
                });
              }
            } else {
              next();
            }
          });
        }
      });
    }
  }
};

exports.log_in = function(req, res, next) {
  var postgreClient = new pg.Client(urlUsuarios);
  postgreClient.connect((err) => {
    if (err) {
      console.error("ERROR CONEXION POSTGRESQL en login:" + err.stack);
      return res.status(503).json({
        message: req.i18n.__('ERROR_CONEXION_BASEDATOS')
      });
    } else {
      var query = postgreClient.query('SELECT * FROM usuarios WHERE usuario = $1', [req.body.email], (err, result) => {
        postgreClient.close;
        if (result.rowCount > 0) {
          if (bcrypt.compareSync(req.body.password, result.rows[0].password)) {
            if (err) {
              console.error("ERROR QUERY SELECT usuarios POSTGRESQL en login:" + err.stack);
              return res.status(500).json({
                message: req.i18n.__('ERROR_SERVIDOR')
              });
            } else {
              next();
            }
          } else {
            return res.status(401).json({
              message: req.i18n.__('ERROR_PASSWORD_LOGIN')
            });
          }

        } else {
          return res.status(401).json({
            message: req.i18n.__('ERROR_USUARIO_LOGIN')
          });
        }
      });
    }
  });
};

exports.change_password = function(req, res) {
  var postgreClient = new pg.Client(urlUsuarios);
  postgreClient.connect((err) => {
    if (err) {
      console.error("ERROR CONEXION POSTGRESQL en change_password:" + err.stack);
      return res.status(503).json({
        message: req.i18n.__('ERROR_CONEXION_BASEDATOS')
      });
    } else {
      var query = postgreClient.query('SELECT * FROM usuarios WHERE usuario = $1', [req.user.email], (err, result) => {
        if (result.rowCount > 0) {
          if (validation.passwordValidation(req.body.newPassword)) {
            if (bcrypt.compareSync(req.body.password, result.rows[0].password)) {
              var newhashedpassword = bcrypt.hashSync(req.body.newPassword, 10);
              var update = postgreClient.query('UPDATE usuarios SET password=($1) WHERE usuario=($2)', [newhashedpassword, req.user.email], (err, result) => {
                postgreClient.close;
                User.findOneAndUpdate({
                  email: req.user.email
                }, {
                  logout: Date.now()
                }, {
                  new: true
                }, function(err, user) {
                  if (err) {
                    console.error("ERROR FINDONEANDUPDATE usuario en change_password:" + err.stack);
                    return res.status(500).json({
                      message: req.i18n.__('ERROR_SERVIDOR')
                    });
                  }
                  if (!user) {
                    return res.status(400).json({
                      message: req.i18n.__('ERROR_USUARIO_LOGIN')
                    });
                  } else {
                    return res.json({
                      token: jwt.sign({
                        exp: Math.floor(Date.now() / 1000) + (60 * 60),
                        userId: user.userId,
                        email: user.email,
                        firstName: user.firstName,
                        lastName: user.lastName,
                        active: user.active,
                        created: user.created,
                        logout: user.logout,
                        _id: user._id
                      }, 'TECHURUTHAUTh')
                    });
                  }
                });
              });
            } else {
              postgreClient.close;
              return res.status(401).json({
                message: req.i18n.__('ERROR_PASSWORD_LOGIN')
              });
            }
          } else {
            postgreClient.close;
            return res.status(400).json({
              message: req.i18n.__('ERROR_DATOS_REGISTRO')
            });
          }
        } else {
          postgreClient.close;
          return res.status(401).json({
            message: req.i18n.__('ERROR_USUARIO_LOGIN')
          });
        }
      });
    }
  });
};
