'use strict';

var mongoose = require('mongoose');
var i18n = require("i18n-2");

var accountUtility = require('../utils/accountUtilities');

var Transaction = mongoose.model('Transaction');
var Account = mongoose.model('Account');

exports.list_all_transactionTypes = function(req, res) {
  var transType = {
    transactionType: [
      {
        codigo:'NOMINA',
        descripcion: req.i18n.__("TIPO_MOVIMIENTO_NOMINA"),
        flow: 'INCOME'
      },
      {
        codigo:'ADEUDO',
        descripcion: req.i18n.__("TIPO_MOVIMIENTO_ADEUDO"),
        flow: 'EXPENSE'
      },
      {
        codigo:'CAJERO',
        descripcion: req.i18n.__("TIPO_MOVIMIENTO_CAJERO"),
        flow: 'EXPENSE'
      },
      {
        codigo:'IMPUESTO',
        descripcion: req.i18n.__("TIPO_MOVIMIENTO_IMPUESTO"),
        flow: 'EXPENSE'
      },
      {
        codigo:'TRANS.REALIZADA',
        descripcion: req.i18n.__("TIPO_MOVIMIENTO_TRANS.REALIZADA"),
        flow: 'EXPENSE'
      },
      {
        codigo:'TRANS.RECIBIDA',
        descripcion: req.i18n.__("TIPO_MOVIMIENTO_TRANS.RECIBIDA"),
        flow: 'INCOME'
      },
      {
        codigo:'VALORES',
        descripcion: req.i18n.__("TIPO_MOVIMIENTO_VALORES"),
        flow: 'INCOME'
      }
    ]
  };
  res.json(transType);
};

exports.create_transaction = function(req, res) {
  Account.findOne({
    userId: req.user.userId,
    accountId: req.params.accountId
  }, ['accountId'], function(err, account) {
    if (!account) {
      return res.status(404).json({
        message: req.i18n.__('ERROR_CUENTA_INEXISTENTE')
      });
    } else {
      //Las cuentas tendrán el formato  ES00 0000 YYYY 0000 XXXX donde YYYY es el código de cliente y XXXX el codigo de cuenta
      var newTransaction = new Transaction(req.body);
      Transaction.findOne({
        accountId: req.params.accountId
      }, ['transactionOrder', 'countUpdatedBalance'], {
        sort: {
          transactionOrder: -1
        }
      }, function(err, lastTransaction) {
        if (err) {
          console.error("ERROR FINDONE create_transaction MONGO: " + err.stack);
          return res.status(500).json({
            message: req.i18n.__('ERROR_SERVIDOR')
          });
        } else {
          if (lastTransaction) {
            //    return res.json(lastAccount);
            newTransaction.transactionOrder = lastTransaction.transactionOrder + 1;
            if (newTransaction.transactionFlowType == 'INCOME') {
              newTransaction.countUpdatedBalance = lastTransaction.countUpdatedBalance + newTransaction.transactionAmount;
            } else if (newTransaction.transactionFlowType == 'EXPENSE') {
              newTransaction.countUpdatedBalance = lastTransaction.countUpdatedBalance - newTransaction.transactionAmount;
            } else {
              return res.status(400).json({
                message: req.i18n.__('ERROR_TIPO_MOVIMIENTO')
              });
            }
          } else {
            if (newTransaction.transactionFlowType == 'INCOME') {
              newTransaction.countUpdatedBalance = newTransaction.transactionAmount;
            } else if (newTransaction.transactionFlowType == 'EXPENSE') {
              newTransaction.countUpdatedBalance = -newTransaction.transactionAmount;
            } else {
              return res.status(400).json({
                message: req.i18n.__('ERROR_TIPO_MOVIMIENTO')
              });
            }
            newTransaction.transactionOrder = 1;
          }
          newTransaction.accountId = req.params.accountId;
          newTransaction.transactionId = accountUtility.completarConCeros(req.params.accountId, 4) + accountUtility.completarConCeros(newTransaction.transactionOrder, 10);
          newTransaction.save(function(err, transaction) {
            if (err) {
              console.error("ERROR SAVE create_transaction MONGO: " + err.stack);
              return res.status(500).json({
                message: req.i18n.__('ERROR_SERVIDOR')
              });
            } else {
              Account.findOneAndUpdate({
                accountId: req.params.accountId
              }, {
                countBalance: newTransaction.countUpdatedBalance
              }, function(err, account) {
                if (err) {
                  console.error("ERROR FINDONEANDUPDATE account en create_transaction MONGO: " + err.stack);
                }
              });
            }
            var tipoMovimiento = "TIPO_MOVIMIENTO_" + transaction.transactionType;
            transaction.transactionType = req.i18n.__(tipoMovimiento);
            return res.status(201).json(transaction);
          });
        }
      });
    }
  });
};

exports.list_all_transactions = function(req, res) {
  Transaction.find({
    accountId: req.params.accountId
  }, [], {
    sort: {
      transactionOrder: -1
    }
  }, function(err, result) {
    if (err) {
      console.error("ERROR FIND EN list_all_transactions MONGO: " + err.stack);
      return res.status(500).json({
        message: req.i18n.__('ERROR_SERVIDOR')
      });
    } else {
      if (result.length != 0) {
        for (var i = 0; i < result.length; i++) {
          var tipoMovimiento = "TIPO_MOVIMIENTO_" + result[i].transactionType;
          result[i].transactionType = req.i18n.__(tipoMovimiento);
        }
        return res.json(result);
      } else {
        return res.status(404).json({
          message: req.i18n.__('ERROR_MOVIMIENTOS_INEXISTENTE')
        });
      }
    }
  });
};

exports.read_transaction = function(req, res) {
  Transaction.findOne({
    accountId: req.params.accountId,
    transactionId: req.params.transactionId
  }, function(err, transaction) {
    if (err) {
      console.error("ERROR FINDONE en read_transaction MONGO: " + err.stack);
      return res.status(500).json({
        message: req.i18n.__('ERROR_SERVIDOR')
      });
    } else {
      if (transaction) {
        var tipoMovimiento = "TIPO_MOVIMIENTO_" + transaction.transactionType;
        transaction.transactionType = req.i18n.__(tipoMovimiento);
        return res.json(transaction);
      } else {
        return res.status(404).json({
          message: req.i18n.__('ERROR_MOVIMIENTO_INEXISTENTE')
        });
      }
    }
  });
}

exports.update_transaction = function(req, res) {
  Transaction.findOneAndUpdate({
    accountId: req.params.accountId,
    transactionId: req.params.transactionId
  }, {
    additionalInformation: req.body.additionalInformation
  }, {
    new: true
  }, function(err, transaction) {
    if (err) {
      console.error("ERROR FINDONEANDUPDATE en update_transaction MONGO: " + err.stack);
      return res.status(500).json({
        message: req.i18n.__('ERROR_SERVIDOR')
      });
    } else {
      if (transaction) {
        var tipoMovimiento = "TIPO_MOVIMIENTO_" + transaction.transactionType;
        transaction.transactionType = req.i18n.__(tipoMovimiento);
        return res.json(transaction);
      } else {
        return res.status(404).json({
          message: req.i18n.__('ERROR_MOVIMIENTO_INEXISTENTE')
        });
      }
    }
  });
};

exports.delete_transaction = function(req, res) {
  //Se hace otro apunte en sentido contrario
  Transaction.findOne({
    accountId: req.params.accountId,
    transactionId: req.params.transactionId
  }, function(err, tdTransaction) {
    if (err) {
      console.error("ERROR FINDONE tdTransaction en delete_transaction MONGO: " + err.stack);
      return res.status(500).json({
        message: req.i18n.__('ERROR_SERVIDOR')
      });
    } else {
      if (tdTransaction) {
        if (tdTransaction.transactionRelated != "") {
          return res.status(400).json({
            message: req.i18n.__('ERROR_MOVIMIENTO_INBORRABLE')
          });
        } else {
          Transaction.findOne({
            accountId: req.params.accountId
          }, ['transactionId', 'transactionOrder', 'countUpdatedBalance'], {
            sort: {
              transactionOrder: -1
            }
          }, function(err, lastTransaction) {
            if (err) {
              console.error("ERROR FINDONE lastTransaction en delete_transaction MONGO: " + err.stack);
              return res.status(500).json({
                message: req.i18n.__('ERROR_SERVIDOR')
              });
            } else {
              if (lastTransaction.transactionId == tdTransaction.transactionId) {
                Transaction.remove({
                  accountId: req.params.accountId,
                  transactionId: req.params.transactionId
                }, function(err, account) {
                  if (err) {
                    console.error("ERROR REMOVE en delete_transaction MONGO: " + err.stack);
                    return res.status(500).json({
                      message: req.i18n.__('ERROR_SERVIDOR')
                    });
                  } else {
                    return res.status(204).send();
                  }
                });
              } else {
                var transaction = new Transaction();
                transaction.accountId = tdTransaction.accountId;
                transaction.transactionAmount = tdTransaction.transactionAmount;
                transaction.transactionType = "ANULACION";
                transaction.transactionRelated = tdTransaction.transactionId;
                transaction.transactionOrder = lastTransaction.transactionOrder + 1;
                transaction.transactionId = accountUtility.completarConCeros(req.params.accountId, 4) + accountUtility.completarConCeros(transaction.transactionOrder, 10);
                if (tdTransaction.transactionFlowType == 'INCOME') {
                  transaction.transactionFlowType = 'EXPENSE';
                  transaction.countUpdatedBalance = lastTransaction.countUpdatedBalance - transaction.transactionAmount;
                } else if (tdTransaction.transactionFlowType == 'EXPENSE') {
                  transaction.transactionFlowType = 'INCOME';
                  transaction.countUpdatedBalance = lastTransaction.countUpdatedBalance + transaction.transactionAmount;
                }
                transaction.save(function(err, newTransaction) {
                  if (err) {
                    console.error("ERROR SAVE en delete_transaction MONGO: " + err.stack);
                    return res.status(500).json({
                      message: req.i18n.__('ERROR_SERVIDOR')
                    });
                  } else {
                    Account.findOneAndUpdate({
                      accountId: req.params.accountId
                    }, {
                      countBalance: newTransaction.countUpdatedBalance
                    }, function(err, account) {
                      if (err) {
                        console.error("ERROR FINDONEANDUPDATE cuenta en delete_transaction logout MONGO: " + err.stack);
                      }
                    });
                    Transaction.findOneAndUpdate({
                      transactionId: tdTransaction.transactionId
                    }, {
                      transactionRelated: newTransaction.transactionId
                    }, function(err, account) {
                      if (err) {
                        console.error("ERROR FINDONEANDUPDATE tdTransaction en delete_transaction MONGO: " + err.stack);
                      }
                    });
                    var tipoMovimiento = "TIPO_MOVIMIENTO_" + newTransaction.transactionType;
                    newTransaction.transactionType = req.i18n.__(tipoMovimiento);
                    return res.json(newTransaction);
                  }
                });
              }
            }
          });
        }
      } else {
        return res.status(404).json({
          message: req.i18n.__('ERROR_MOVIMIENTO_INEXISTENTE')
        });
      }
    }
  });
};

exports.delete = function(req, res) {
  Transaction.remove({
    accountId: req.params.accountId
  }, function(err, transaction) {
    if (err) {
      console.error("ERROR REMOVE en delete_transaction MONGO: " + err.stack);
    }
  });
};
