var express = require('express');
var app = express();
var port = process.env.PORT || 3000;


var mongoose = require('mongoose');
var bodyParser = require('body-parser');
var jwt = require('jsonwebtoken');
var i18n = require("i18n-2");
var cors = require('cors');

var User = require('./api/models/userModel');
var Account = require('./api/models/accountModel');
var Transaction = require('./api/models/transactionModel');

var promiseLib = global.Promise;
var mongoUri = 'mongodb://admin:admin@ds046047.mlab.com:46047/dbtechurcl';
mongoose.Promise = promiseLib;
mongoose.connect(mongoUri, {
        useMongoClient: true,
        promiseLibrary: promiseLib
    });

i18n.expressBind(app, {
  locales: ['es', 'en'],
  directory: __dirname + '/api/locales',
});

app.use(cors());
app.use(function(req, res, next) {
  req.i18n.setLocaleFromQuery();
  next();
});

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use(function(req, res, next){
  if (req.headers && req.headers.authorization && req.headers.authorization.split(' ')[0] === "RCLAPI") {
      jwt.verify(req.headers.authorization.split(' ')[1], 'TECHURUTHAUTh', function (err, decode) {
        if (err) req.user = undefined;
        req.user = decode;
        next();
      });
  } else {
    req.user = undefined;
    next();
  }
});

var routes = require('./api/routes/myAPIRoutes'); //importing route
routes(app); //register the route


app.listen(port);

app.use(function(req, res) {
  res.status(404).send({url: req.originalUrl + ' not found'})
});

console.log('TechU RESTful API server started on: ' + port);
